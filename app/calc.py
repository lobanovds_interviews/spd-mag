from mypy.binder import Union

from app.operation import Operation


class Calc(object):
    def __init__(self, x: int, y: int, operation: Operation):
        self.x: int = x
        self.y: int = y
        self.operation: Operation = operation

    def operate(self) -> Union[int, float]:
        if self.operation == Operation.SUM:
            return self.add()
        if self.operation == Operation.DIFFERENCE:
            return self.difference()
        if self.operation == Operation.MULTIPLY:
            return self.multiply()
        if self.operation == Operation.DIVIDE:
            return self.divide()
        raise ValueError

    def add(self) -> int:
        return self.x + self.y

    def difference(self) -> int:
        return self.x - self.y

    def multiply(self) -> int:
        return self.x * self.y

    def divide(self) -> float:
        if self.y == 0:
            raise ZeroDivisionError
        return self.x / self.y


if __name__ == "__main__":
    pass
