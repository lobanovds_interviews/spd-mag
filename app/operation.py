from enum import Enum


class Operation(Enum):
    SUM = "+"
    DIFFERENCE = "-"
    MULTIPLY = "*"
    DIVIDE = "/"


if __name__ == "__main__":
    pass
