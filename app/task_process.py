from os import environ
from time import sleep

from app.task_state import TaskState


class TaskProcess(object):
    def __init__(self, proc_id, command):
        self.id = proc_id
        self.state = TaskState.CREATED
        self.result = None
        self.command = command

    def produce(self) -> None:
        self.state = TaskState.PROCESSING
        if "DEVELOPMENT" in environ and environ["DEVELOPMENT"]:
            sleep(2)
        try:
            self.result = self.command()
        except ZeroDivisionError:
            print(str(self.command))
            self.state = TaskState.KILLED
            return
        self.state = TaskState.COMPLETED
