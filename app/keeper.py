from typing import Dict, Generator, Union, Optional, Any

from app.task_process import TaskProcess


class Keeper(object):
    def __init__(self):
        self.data: list = []

    @property
    def counter(self) -> int:
        return len(self.data)

    def add_data(self, command) -> Generator[TaskProcess, None, None]:
        process = TaskProcess(self.counter, command)
        self.data.append(process)
        yield process

    def get_data(self, addr) -> TaskProcess:
        if self.counter < addr:
            raise ValueError
        return self.data[addr]

    def get_all_data(self) -> Generator[Optional[Dict], None, None]:
        counter: int = 0
        while True:
            result: Union[Dict, None] = None
            if counter < len(self.data):
                returned_process: TaskProcess = self.data[counter]
                result = {
                    "task_id": returned_process.id,
                    "result": returned_process.result,
                    "status": returned_process.state.value,
                }
                counter += 1
            yield result


if __name__ == "__main__":
    pass
