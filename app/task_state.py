from enum import Enum


class TaskState(Enum):
    CREATED = "created"
    PROCESSING = "processing"
    COMPLETED = "completed"
    KILLED = "killed"


if __name__ == "__main__":
    pass
