#!/usr/bin/env python3
import os
from asyncio import sleep
from json import JSONDecodeError
from logging import DEBUG, getLogger
from typing import Union

from fastapi import BackgroundTasks, FastAPI, HTTPException, Request, WebSocket
from fastapi.responses import JSONResponse, Response
from fastapi.templating import Jinja2Templates
from pydantic import ValidationError
from starlette.status import (
    HTTP_200_OK,
    HTTP_422_UNPROCESSABLE_ENTITY,
    HTTP_500_INTERNAL_SERVER_ERROR,
)
from uvicorn import run

from app.calc import Calc
from app.keeper import Keeper
from app.validator import CalcModel

logger = getLogger("app")
logger.setLevel(DEBUG)

templates = Jinja2Templates(directory="app/templates")
app = FastAPI(debug=True)
task_data = Keeper()


@app.post("/calc")
async def calc(
    request: Request, background_tasks: BackgroundTasks
) -> Union[JSONResponse, HTTPException]:
    try:
        calc_model = CalcModel.validate(await request.json())
        calcus = Calc(calc_model.x, calc_model.y, calc_model.operation)
        operator = task_data.add_data(calcus.operate)
        calc_process = next(operator)
        background_tasks.add_task(calc_process.produce)
        return JSONResponse(
            content={"process_id": calc_process.id}, status_code=HTTP_200_OK
        )
    except ValidationError as ve:
        return HTTPException(
            status_code=HTTP_422_UNPROCESSABLE_ENTITY, detail=repr(ve.errors())
        )
    except JSONDecodeError:
        return HTTPException(
            status_code=HTTP_422_UNPROCESSABLE_ENTITY, detail="JSON Decode error"
        )
    except Exception as msg:
        logger.error(msg)
        return HTTPException(status_code=HTTP_500_INTERNAL_SERVER_ERROR, detail=msg)


@app.get("/result/{task_id}")
async def result(task_id: int) -> Union[JSONResponse, HTTPException]:
    try:
        task_proc = task_data.get_data(task_id)
        return JSONResponse(
            content={"result": task_proc.result, "status": task_proc.state.value},
            status_code=200,
        )
    except IndexError:
        return HTTPException(
            status_code=HTTP_422_UNPROCESSABLE_ENTITY, detail="Nothing to show"
        )
    except ValueError:
        return HTTPException(
            status_code=HTTP_422_UNPROCESSABLE_ENTITY, detail="Wrong task_id"
        )
    except Exception as msg:
        logger.error(msg)
        return HTTPException(status_code=HTTP_500_INTERNAL_SERVER_ERROR, detail=msg)


@app.get("/")
def show_tasks(request: Request) -> Response:
    a = templates.TemplateResponse(
        "index.html",
        context={
            "request": request,
            "websocket_host": os.environ["APP_HOST"],
            "websocket_port": os.environ["APP_PORT"],
        },
    )
    return a


@app.websocket("/ws")
async def websocket_endpoint(websocket: WebSocket):
    await websocket.accept()
    data = task_data.get_all_data()
    while True:
        payload = next(data)
        if payload is not None:
            logger.debug(payload["task_id"])
            await websocket.send_json(payload)
        await sleep(0.02)


if __name__ == "__main__":
    run(
        app=app,
        host=os.environ["APP_HOST"],
        port=int(os.environ["APP_PORT"]),
        log_level="debug",
        debug=True,
    )
