from pydantic import BaseModel, ValidationError, validator

from app.operation import Operation


class CalcModel(BaseModel):
    x: int
    y: int
    operation: Operation

    @classmethod
    @validator("y")
    def divide_by_zero(cls, y, params):
        if params["operation"] == Operation.DIVIDE and y == 0:
            raise ValidationError
        return y


if __name__ == "__main__":
    pass
