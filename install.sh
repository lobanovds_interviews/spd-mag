#!/usr/bin/env bash

sudo rm -rf /tmp/lobanovds
mkdir -p /tmp/lobanovds/

sudo apt update -y && sudo -H apt install docker docker-compose
curl -o /tmp/lobanovds/docker-compose.yaml https://gitlab.com/lobanovds_interviews/spd-mag/-/raw/main/docker-compose.yaml?inline=false
~/.local/bin/docker-compose down || /usr/bin/docker-compose down
~/.local/bin/docker-compose -f /tmp/lobanovds/docker-compose.yaml up -d || /usr/bin/docker-compose -f /tmp/lobanovds/docker-compose.yaml up -d