import json

import pytest
from pydantic import ValidationError

from app.operation import Operation
from app.validator import CalcModel


@pytest.mark.parametrize(
    "x, y, operation",
    [
        (1, 1, Operation.DIVIDE),
        (2, 1, Operation.SUM),
        (-100, 2, Operation.DIFFERENCE),
        (0, 1, Operation.MULTIPLY),
    ],
)
def test_validation_right(x, y, operation):
    try:
        CalcModel().validate(json.dumps({"x": x, "y": y, "operation": operation}))
        assert False
    except ValidationError:
        assert True


@pytest.mark.parametrize(
    "x, y, operation",
    [
        ("1", 1, Operation.DIVIDE),
        (2, "1", Operation.SUM),
        (-100, 2, "Operation.DIFFERENCE"),
        (0, None, Operation.MULTIPLY),
    ],
)
def test_validation_wrong(x, y, operation):
    try:
        CalcModel().validate(
            json.dumps({"x": 1, "y": 0, "operation": Operation.DIVIDE})
        )
        assert False
    except ValidationError:
        assert True
