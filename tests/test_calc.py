#!/usr/bin/env python3
import pytest

from app.calc import Calc
from app.operation import Operation


def test_divide_exception():
    test_object = Calc(x=1, y=0, operation=Operation.DIVIDE)
    try:
        test_object.divide()
    except ZeroDivisionError:
        assert True


@pytest.mark.parametrize(
    "x, y, expected", [(1, 1, 1.0), (2, 1, 2.0), (-100, 2, -50.0), (0, 1, 0.0)]
)
def test_divide(x, y, expected):
    test_object = Calc(x=x, y=y, operation=Operation.DIVIDE)
    assert test_object.divide() == expected


@pytest.mark.parametrize(
    "x, y, expected", [(0, 0, 0), (1, 1, 2), (2, 1, 3), (-100, 2, -98)]
)
def test_add(x, y, expected):
    test_object = Calc(x=x, y=y, operation=Operation.SUM)
    assert test_object.add() == expected


@pytest.mark.parametrize(
    "x, y, expected", [(0, 0, 0), (0, 10, -10), (2, 1, 1), (500, 500, 0)]
)
def test_difference(x, y, expected):
    test_object = Calc(x=x, y=y, operation=Operation.DIFFERENCE)
    assert test_object.difference() == expected


@pytest.mark.parametrize(
    "x, y, expected",
    [(0, 0, 0), (1, 1, 1), (0, -1, 0), (1, -1, -1), (-1, -1, 1), (-1, 1, -1)],
)
def test_multiply(x, y, expected):
    test_object = Calc(x=x, y=y, operation=Operation.MULTIPLY)
    assert test_object.multiply() == expected


@pytest.mark.parametrize(
    "x, y, operation",
    [(0, 0, Operation.DIVIDE), (-1, 0, Operation.DIVIDE), (100, 0, Operation.DIVIDE)],
)
def test_operate_div_by_zero(x, y, operation):
    test_object = Calc(x=x, y=y, operation=Operation.DIVIDE)
    try:
        test_object.divide()
    except ZeroDivisionError:
        assert True


@pytest.mark.parametrize(
    "x, y, operation, expected",
    [
        (0, 1, Operation.MULTIPLY, 0),
        (-1, 0, Operation.MULTIPLY, 0),
        (100, 0, Operation.MULTIPLY, 0),
        (1, 1, Operation.MULTIPLY, 1),
        (-1, 1, Operation.MULTIPLY, -1),
        (1, -1, Operation.MULTIPLY, -1),
        (1, 0, Operation.MULTIPLY, 0),
        (0, 0, Operation.MULTIPLY, 0),
        (10, 10, Operation.MULTIPLY, 100),
        (0, 0, Operation.SUM, 0),
        (-1, 1, Operation.SUM, 0),
        (1, -1, Operation.SUM, 0),
        (1, 1, Operation.SUM, 2),
        (10, 10, Operation.SUM, 20),
        (-10, -1, Operation.SUM, -11),
        (0, 0, Operation.DIFFERENCE, 0),
        (-1, 1, Operation.DIFFERENCE, -2),
        (1, -1, Operation.DIFFERENCE, 2),
        (1, 1, Operation.DIFFERENCE, 0),
        (10, 10, Operation.DIFFERENCE, 0),
        (-10, -1, Operation.DIFFERENCE, -9),
        (0, 1, Operation.DIVIDE, 0.0),
        (1, 2, Operation.DIVIDE, 0.5),
        (1, 1, Operation.DIVIDE, 1.0),
        (-10, 2, Operation.DIVIDE, -5.0),
    ],
)
def test_operate(x, y, operation, expected):
    test_object = Calc(x=x, y=y, operation=operation)
    assert test_object.operate() == expected
