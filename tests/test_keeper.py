from random import randint

from app.calc import Calc
from app.keeper import Keeper
from app.operation import Operation


def test_counter():
    keeper: Keeper = Keeper()
    for i in range(50):
        test_object = Calc(x=randint(-0, 1000), y=1, operation=Operation.SUM)
        test_generator = keeper.add_data(test_object.operate)
        next(test_generator)
    assert keeper.counter == 50
