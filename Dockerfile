FROM python:3.8-slim-buster

COPY ./pytest.ini ./requirements.txt /
RUN apt update -y && apt upgrade -y && pip3 install -Ur /requirements.txt

ADD ./ /



